# Light Theme

* Theme for BlizzCMS-Plus.

[![Project Status](https://img.shields.io/badge/Status-In_Development-yellow.svg?style=flat-square)](#)
[![Project Version](https://img.shields.io/badge/Version-1.0.1-green.svg?style=flat-square)](#)


# Project

* https://wow-cms.com/en/
* https://gitlab.com/WoW-CMS

# Installation

* Add the "Light" folder in /application/themes/
* Then go to the admin panel and rename the theme to "light" save changes and you're good to go.

# Requirements

* BlizzCMS-Plus

## Copyright

Copyright © 2019 [WoW-CMS](https://wow-cms.com).

## Warning

* The credits of this theme cannot be removed by the user, in that case it will no longer receive support from the developers.

* This theme cannot be distributed or shared unless you download it directly from this site. In this case, support will be discontinued and future updates will not have access to the repository.


# Screenshots

![Screenshot](Screenshot.png)