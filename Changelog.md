## 1.0.1


### Changed

- Degradation from red to blue in card status.
- Update Social network icons with their respective colors.
- Update Navbar to Sticky navigation bar.

### Added

- Modules files, with other class css.
- Forum animation.

### Removed

- Remove player count Online in server status.
- Header section removed.
